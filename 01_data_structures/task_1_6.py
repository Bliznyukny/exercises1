# -*- coding: utf-8 -*-
'''
Задание 1.6
Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20
Ограничение: Все задания надо выполнять используя только пройденные темы.
'''
vova = 'O        15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'
ip='10.0.13.3'
l=len(ip)
n= vova.find(ip, 1)
ipv= (vova[n:l+n])

g='Moscow'
lg=len(g)
ng= vova.find(g, 1)
gv= (vova[ng:lg+ng])
d='15.03.2020'
ld=len(d)
nd= vova.find(d, 1)
dv= (vova[nd:ld+nd])

t='15:20'
lt=len(t)
nt= vova.find(t, 1)
tv= (vova[nt:lt+nt])

print ("{:15}".format ('name:'), "Владимир")
print ("{:15}".format ('ip:'), ipv)
print ("{:15}".format ('город:'), gv)
print ("{:15}".format ('date:'), dv)
print ("{:15}".format ('time:'), tv)