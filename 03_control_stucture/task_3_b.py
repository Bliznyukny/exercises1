

'''
Задание 3.1b
Скопировать код из предыдущего задания
Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения;
    - при вводе пустой строки программа останавливалась.
'''
while True:
    calc=input("Введите выражение : " )
    if not calc:
        break
    elif calc.find('+',0, len(calc)) >=  0 :
        z = calc.partition('+')
        v1 = int(z[0])
        v2 = int(z[2])
        z = str(z[1])
        # print(v1, v2, z)
        print('Результат сложения ', calc, ' : ', v1 + v2)
    elif calc.find('-',0, len(calc)) >= 0 :
        z = calc.partition('-')
        v1 = int(z[0])
        v2 = int(z[2])
        z = str(z[1])
        # print(v1, v2, z)
        print('Результат вычетания', calc, ' : ', v1 - v2)
    elif calc.find('*', 0, len(calc)) >= 0:
        z = calc.partition('*')
        v1 = int(z[0])
        v2 = int(z[2])
        z = str(z[1])
        # print(v1, v2, z)
        print('Результат умножения', calc, ' : ', v1 * v2)
    elif calc.find('*', 0, len(calc)) >= 0:
        z = calc.partition('*')
        v1 = int(z[0])
        v2 = int(z[2])
        z = str(z[1])
        # print(v1, v2, z)
        print('Результат деления', calc, ' : ', v1 / v2)
    else: print("Невалидное выражение")





